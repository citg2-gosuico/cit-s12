package com.zuitt.discussion.services;

import com.zuitt.discussion.models.Post;
import org.springframework.http.ResponseEntity;

public interface PostService {
//  Create post
    void createPost(Post post);

//    viewing all posts
    Iterable<Post> getPosts();

//    delete a post
    ResponseEntity deletePost(Long id);

//    update a post
    ResponseEntity updatePost(Long id, Post post);
}

