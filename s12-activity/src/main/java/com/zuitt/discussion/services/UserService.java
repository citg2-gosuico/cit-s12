package com.zuitt.discussion.services;

import com.zuitt.discussion.models.Post;
import com.zuitt.discussion.models.User;
import org.springframework.http.ResponseEntity;

public interface UserService {

//    create user
    void createUser(User user);

//    get all users
    Iterable<User> getUsers();

    //    delete a user
    ResponseEntity deleteUser(Long id);

    //    update a user
    ResponseEntity updateUser(Long id, User user);


}
